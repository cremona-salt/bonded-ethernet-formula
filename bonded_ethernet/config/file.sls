# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import bonded_ethernet with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

bonded-ethernet-config-file-netbond-config-managed:
  file.managed:
    - name: /etc/netplan/02-netbond.yaml
    - source: {{ files_switch(['netbond.yml.tmpl'],
                              lookup='bonded-ethernet-config-file-netbond-config-managed',
                 )
              }}
    - mode: 644
    - user: root
    - group: root
    - makedirs: True
    - template: jinja
    - context:
        interfaces: {{ bonded_ethernet.get('interfaces', []) | yaml }}
